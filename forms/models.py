from django.db import models

SMT_CHOICES = [
    ('Gasal (2019/2020)', 'Gasal (2019/2020)'),
    ('Genap (2019/2020)', 'Genap (2019/2020)'),
    ('Gasal (2020/2021)', 'Gasal (2020/2021)'),
    ('Genap (2020/2021', 'Genap (2020/2021)'),
]

class Matkul(models.Model):
    name = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.PositiveSmallIntegerField()
    desc = models.TextField(max_length=200)
    smt = models.CharField(max_length=17, choices=SMT_CHOICES)
    ruang = models.PositiveSmallIntegerField()