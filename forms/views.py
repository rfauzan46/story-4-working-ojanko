from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Matkul
# Create your views here.


def forms(request):
    response = {'input_form': Input_Form}
    return render(request,'forms.html', response)

def savename(request):
    form = Input_Form(request.GET or None)
    if (form.is_valid and request.method == 'GET'):
        form.save()
        return HttpResponseRedirect('./readmatkul')
    else:
        return HttpResponseRedirect('/')

def readmatkul(request):
    matkuls = Matkul.objects.all()
    response = {'matkuls' : matkuls}
    html = 'forms.html'
    return render(request, html, response)