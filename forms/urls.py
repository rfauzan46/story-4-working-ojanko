from django.urls import path, include
from .views import forms, savename, readmatkul

app_name = "forms"

urlpatterns = [
    path('', forms, name='forms'),
    path('savename', savename, name='savename'),
    path('readmatkul', readmatkul, name='readmatkul')
]
