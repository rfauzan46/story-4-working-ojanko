from django import forms
from .models import Matkul

SMT_CHOICES = [
    ('Gasal (2019/2020)', 'Gasal (2019/2020)'),
    ('Genap (2019/2020)', 'Genap (2019/2020)'),
    ('Gasal (2020/2021)', 'Gasal (2020/2021)'),
    ('Genap (2020/2021', 'Genap (2020/2021)'),
]

class Input_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['name', 'dosen', 'sks', 'desc', 'smt', 'ruang']

    error_messages = {
        'required' : 'Tolong diisi'
    }

    name_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Nama Mata Kuliah'
    }

    dosen_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Nama Dosen'
    }

    sks_attrs = {
        'type':'number',
        'class':"form-control",
        'placeholder':'Jumlah SKS'
    }

    desc_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Deskripsi Mata Kuliah'
    }

    smt_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Semester'
    }

    ruang_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Ruang Belajar'
    }

    name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    dosen = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=dosen_attrs))
    sks = forms.IntegerField(label='', required=True, widget=forms.TextInput(attrs=sks_attrs))
    desc = forms.CharField(label='', required=True, max_length=200, widget=forms.Textarea(attrs=desc_attrs))
    smt = forms.CharField(label='', required=True, max_length=17, widget=forms.Select(choices=SMT_CHOICES))
    ruang = forms.IntegerField(label='', required=True, widget=forms.TextInput(attrs=ruang_attrs))
