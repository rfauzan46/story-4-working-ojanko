from django.urls import path, include
from .views import story7

app_name = "story7"

urlpatterns = [
    path('', story7, name='story7'),
]