from django.urls import path, include
from .views import forms

app_name = "searching"

urlpatterns = [
    path('', forms, name='searching'),
]