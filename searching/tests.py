from django.test import TestCase, Client

# Create your tests here.
class SearchingTestCase(TestCase):
	def test_keberadaan_searching(self):
		response = Client().get('/searching/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_kegiatan_sesuai_template(self):
		response = Client().get('/searching/')
		self.assertTemplateUsed(response, 'searching.html')
