from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')
def education(request):
    return render(request, 'education.html')
def theman(request):
    return render(request, 'theman.html')
def specialties(request):
    return render(request, 'specialties.html')
def experiences(request):
    return render(request, 'experiences.html')
def contacts(request):
    return render(request, 'contacts.html')