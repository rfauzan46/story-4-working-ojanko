from django.urls import path, include
from .views import index, education, theman, specialties, experiences, contacts

urlpatterns = [
    path('', index, name='index'),
    path('theman.html/', theman, name='theman'),
    path('education.html/', education, name='education'),
    path('experiences.html/', experiences, name='experiences'),
    path('specialties.html/', specialties, name='specialties'),
]
