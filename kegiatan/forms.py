from django import forms
from .models import Kegiatan, Peserta

class Form_Kegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['kgt']

    error_messages = {
        'required' : 'Tolong diisi'
    }

    kgt_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Nama Kegiatan'
    }

    kgt = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=kgt_attrs))

class Form_Peserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['pst']

    error_messages = {
        'required' : 'Tolong diisi'
    }
    
    pst_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Nama Peserta'
    }
    pst = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=pst_attrs))