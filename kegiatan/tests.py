from django.test import TestCase, Client
from django.urls import resolve
from .views import  kegiatan, formKgt, formPst, delete
from .models import Kegiatan, Peserta

# Create your tests here.
class KegiatanTestCase(TestCase):
	def test_keberadaan_kegiatan(self):
		response = Client().get('/kegiatan/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_kegiatan_sesuai_template(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'kegiatan.html')

class TestTambahKegiatan(TestCase):
	def test_keberadaan_tambahkgt(self):
		response = Client().get('/kegiatan/buatkgt/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_tambahkgt_sesuai_template(self):
		response = Client().get('/kegiatan/buatkgt/')
		self.assertTemplateUsed(response, 'buatkgt.html')
	def test_object_baru_masuk(self):
		kegiatan = Kegiatan(kgt="Bimbel")
		kegiatan.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)
	def test_url_post_bisa(self):
		response = Client().post('/kegiatan/', data={'kgt': 'Valorant'})
		self.assertEqual(response.status_code, 200)

class TestTambahPeserta(TestCase):
	def setUp(self):
		kegiatan = Kegiatan(kgt="Bimbel")
		kegiatan.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)
	def test_tambahpst_POST(self):
		response =  Client().post('/kegiatan/buatpst/1', data={'pst': 'Mang Oleh'})
		self.assertEquals(response.status_code, 302)
	def test_tambahpst_GET(self):
		response = self.client.get('/kegiatan/buatpst/1')
		self.assertTemplateUsed(response, 'buatpst.html')
		self.assertEqual(response.status_code, 200)

class TestHapusKegiatan(TestCase):
	def setUp(self):
		mabar = Kegiatan(kgt="Mabar")
		mabar.save()
		orang = Peserta(pst="Agan", kgt_pst=mabar)
		orang.save()

	def test_keberadaan_url_hapus(self):
		response = Client().post('/kegiatan/hapus/1/')
		self.assertEqual(response.status_code, 302)


