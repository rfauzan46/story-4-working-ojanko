from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form_Kegiatan, Form_Peserta
from .models import Kegiatan, Peserta

# Create your views here.
def formKgt(request):
	form = Form_Kegiatan(request.POST or None)
	if (form.is_valid and request.method == "POST"):
		form.save()
		return HttpResponseRedirect('/kegiatan')
	response = {'input_form': Form_Kegiatan}
	return render(request, 'buatkgt.html', response)

def formPst(request,id_kegiatan):
	form = Form_Peserta(request.POST or None)
	if (form.is_valid and request.method == "POST"):
		nama = Peserta(kgt_pst=Kegiatan.objects.get(id=id_kegiatan), pst=form.data['pst'])
		nama.save()
		return HttpResponseRedirect('/kegiatan')
	response = {'input_form': Form_Peserta}
	return render(request, 'buatpst.html', response)

def kegiatan(request):
	isi_kegiatan = Kegiatan.objects.all()
	isi_peserta = Peserta.objects.all()
	response = {
		'isi_kegiatan': isi_kegiatan,
		'isi_peserta': isi_peserta
	}
	return render(request, 'kegiatan.html', response)

def delete(request,id_kegiatan):
    Kegiatan.objects.filter(id=id_kegiatan).delete()
    return HttpResponseRedirect('/kegiatan')
