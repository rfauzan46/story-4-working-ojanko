from django.urls import path, include
from .views import kegiatan, formKgt, formPst, delete

app_name = "kegiatan"

urlpatterns = [
    path('', kegiatan, name='kegiatan'),
    path('buatkgt/', formKgt, name='formKgt'),
    path('buatpst/<int:id_kegiatan>', formPst, name='formPst'),
    path('hapus/<int:id_kegiatan>/', delete, name='delete'),
]