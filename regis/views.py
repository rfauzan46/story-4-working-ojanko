from django.shortcuts import render
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from .forms import Register, Login
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your views here.
def home(request):
    return render(request,'welcome.html')

def register(request):
    form = Register()
    if request.method == "POST":
        form_input = Register(request.POST)
        if form_input.is_valid():
            uname = request.POST['username']
            pw = request.POST['password1']
            new_user = User.objects.create_user(username=uname, email=request.POST['email'], password=pw)
            new_user.save()
            user_login = authenticate(request, username=uname, password=pw)
            django_login(request, user_login)
            return HttpResponseRedirect('/regis')
        else:
            return render(request, 'register.html', {'form': form_input})
    return render(request, 'register.html', {'form': form})

def login(request):
    form = Login()
    if request.method == "POST":
        data = Login(request.POST)
        if data.is_valid():
            uname = request.POST['username']
            pw = request.POST['password']
            user_login = authenticate(request, username=uname, password=pw)
            if user_login is None:
                return render(request, 'login.html', {'status': 'failed', 'form': form})
            else:
                django_login(request, user_login)
                return HttpResponseRedirect('/regis')
        else:
            return render(request, 'login.html', {'status': 'failed', 'form': data})
    else:
        return render(request, 'login.html', {'form': form})

@login_required
def logout(request):
    django_logout(request)
    return HttpResponseRedirect('/regis')


