from django.urls import path, include
from .views import home, register, login, logout

app_name = "regis"

urlpatterns = [
    path('', home, name='regis'),
    path('register/', register, name="register"),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),

]