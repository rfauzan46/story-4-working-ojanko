from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
class TestTemplate(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/regis/')
        self.assertEqual(response.status_code, 200)

    def test_event_using_template(self):
        template = Client().get('/regis/')
        self.assertTemplateUsed(template, 'welcome.html')

class TestLogIn(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/regis/login/')
        self.assertEqual(response.status_code, 200)

    def test_event_using_template(self):
        template = Client().get('/regis/login/')
        self.assertTemplateUsed(template, 'login.html')

    def test_signin_with_new_account(self):
        user = User.objects.create(username='dadang', email='dadang@dading.com')
        user.set_password('mangoleh')
        user.save()
        logged_in = Client().login(username='dadang', password='mangoleh')
        self.assertTrue(logged_in)

    def test_login_POST(self):
        response = Client().post('/regis/login/', {'username': 'dadang', 'password': 'mangoleh'})
        self.assertEqual(response.status_code, 200)


class TestSignIn(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/regis/register/')
        self.assertEqual(response.status_code, 200)

    def test_event_using_template(self):
        template = Client().get('/regis/register/')
        self.assertTemplateUsed(template, 'register.html')
    
    def test_login_POST(self):
        response = Client().post('/regis/register/', {'username': 'dadang', 'email':'dadang@dading.com', 'password1': 'mangoleh', 'password2': 'mangoleh'})
        self.assertEqual(response.status_code, 302)

class TestLogOut(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/regis/logout/')
        self.assertEqual(response.status_code, 302)