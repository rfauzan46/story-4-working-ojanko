from django.urls import path, include
from .views import page

app_name = "old"

urlpatterns = [
    path('', page, name='page'),
]