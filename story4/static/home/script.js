$( function() {
    $( "#accordion" )
    .accordion({
        header: "> div > #header", collapsible:true
      });
      $(".moveup").on("click", function() {
        var elem = $(this).closest("div");
        elem.prev().before(elem);
    });
    
    $(".movedown").on("click", function() {
        var elem = $(this).closest("div");
        elem.next().after(elem);
    });
  } );

